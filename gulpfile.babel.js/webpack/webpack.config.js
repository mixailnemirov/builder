export const webpackConfig = {
  mode:   process.env.NODE_ENV || 'development',
  entry:  {
    main:  './src/scripts/main.js',
    index: './src/scripts/index.js'
  },
  output: {
    filename: '[name].bundle.js'
  }
};